#include "iomax.h"
#include "unity.h"
#include <stdbool.h>
#include <string.h>
#include <stdint.h>

/* STUBS */
void delay_ns(uint32_t ns){__NOP();}
/* STUBS */

void setUp(void)
{
}

void tearDown(void)
{
}

void test_read(void)
{
	BUS = 0xBBBB;
	uint16_t ret = iomax_read_adc();
	TEST_ASSERT_EQUAL(0xBBBB, ret);
	TEST_ASSERT_EQUAL(1, IOR_READ()); // after write ior must be 1
	TEST_ASSERT_EQUAL(1, IOW_READ()); // after write iow must be 1
	ret = iomax_read_cabin_state_and_reset_adc(); // 8 bit
	TEST_ASSERT_EQUAL(0xBB, ret);
	TEST_ASSERT_EQUAL(1, IOR_READ()); // after write ior must be 1
	TEST_ASSERT_EQUAL(1, IOW_READ()); // after write iow must be 1
	ret = iomax_read_discrete_input();
	TEST_ASSERT_EQUAL(0xBBBB, ret);
	TEST_ASSERT_EQUAL(1, IOR_READ()); // after write ior must be 1
	TEST_ASSERT_EQUAL(1, IOW_READ()); // after write iow must be 1
}

void test_write(void)
{
	iomax_start_adc(0xAAAA);
	TEST_ASSERT_EQUAL(0xAAAA, BUS);
	TEST_ASSERT_EQUAL(1, IOW_READ()); // after write iow must be 1
	TEST_ASSERT_EQUAL(1, IOR_READ()); // after write ior must be 1

	iomax_write_tiristor(0xCC);
	TEST_ASSERT_EQUAL(0xCC, BUS);
	TEST_ASSERT_EQUAL(1, IOW_READ()); // after write iow must be 1
	TEST_ASSERT_EQUAL(1, IOR_READ()); // after write ior must be 1
	
	iomax_write_discrete_output(0xDDDD);
	TEST_ASSERT_EQUAL(0xDDDD, BUS);
	TEST_ASSERT_EQUAL(1, IOW_READ()); // after write iow must be 1
	TEST_ASSERT_EQUAL(1, IOR_READ()); // after write ior must be 1
}

int main(void)
{
    UNITY_BEGIN();
	RUN_TEST(test_read);
	RUN_TEST(test_write);
    return UNITY_END();
}
