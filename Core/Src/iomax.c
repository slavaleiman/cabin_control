#include "iomax.h"
#include "stdint.h"
#include "delay.h"

typedef enum
{
	BUS_NOT_INITED,
	BUS_INPUT,
	BUS_OUTPUT
}bus_state_t; 

struct
{
	uint8_t DK; // cabin selector
	bus_state_t bus_state;
}iomax;

void bus_write(uint16_t out)
{
	if(iomax.bus_state != BUS_OUTPUT)
	{
		BUS_CONFIGURE_OUTPUT();
		iomax.bus_state = BUS_OUTPUT;
	}
 	BUS_WRITE_PIN(DR00_PORT, DR00_PIN, (out & (1 << 0)) ? GPIO_PIN_SET : GPIO_PIN_RESET);
 	BUS_WRITE_PIN(DR01_PORT, DR01_PIN, (out & (1 << 1)) ? GPIO_PIN_SET : GPIO_PIN_RESET);
 	BUS_WRITE_PIN(DR02_PORT, DR02_PIN, (out & (1 << 2)) ? GPIO_PIN_SET : GPIO_PIN_RESET);
 	BUS_WRITE_PIN(DR03_PORT, DR03_PIN, (out & (1 << 3)) ? GPIO_PIN_SET : GPIO_PIN_RESET);
 	BUS_WRITE_PIN(DR04_PORT, DR04_PIN, (out & (1 << 4)) ? GPIO_PIN_SET : GPIO_PIN_RESET);
 	BUS_WRITE_PIN(DR05_PORT, DR05_PIN, (out & (1 << 5)) ? GPIO_PIN_SET : GPIO_PIN_RESET);
 	BUS_WRITE_PIN(DR06_PORT, DR06_PIN, (out & (1 << 6)) ? GPIO_PIN_SET : GPIO_PIN_RESET);
 	BUS_WRITE_PIN(DR07_PORT, DR07_PIN, (out & (1 << 7)) ? GPIO_PIN_SET : GPIO_PIN_RESET);
 	BUS_WRITE_PIN(DR08_PORT, DR08_PIN, (out & (1 << 8)) ? GPIO_PIN_SET : GPIO_PIN_RESET);
 	BUS_WRITE_PIN(DR09_PORT, DR09_PIN, (out & (1 << 9)) ? GPIO_PIN_SET : GPIO_PIN_RESET);
 	BUS_WRITE_PIN(DR10_PORT, DR10_PIN, (out & (1 << 10)) ? GPIO_PIN_SET : GPIO_PIN_RESET);
 	BUS_WRITE_PIN(DR11_PORT, DR11_PIN, (out & (1 << 11)) ? GPIO_PIN_SET : GPIO_PIN_RESET);
 	BUS_WRITE_PIN(DR12_PORT, DR12_PIN, (out & (1 << 12)) ? GPIO_PIN_SET : GPIO_PIN_RESET);
 	BUS_WRITE_PIN(DR13_PORT, DR13_PIN, (out & (1 << 13)) ? GPIO_PIN_SET : GPIO_PIN_RESET);
 	BUS_WRITE_PIN(DR14_PORT, DR14_PIN, (out & (1 << 14)) ? GPIO_PIN_SET : GPIO_PIN_RESET);
 	BUS_WRITE_PIN(DR15_PORT, DR15_PIN, (out & (1 << 15)) ? GPIO_PIN_SET : GPIO_PIN_RESET);
}

uint16_t bus_read(void)
{
	if(iomax.bus_state != BUS_INPUT)
	{
		BUS_CONFIGURE_INPUT();
		iomax.bus_state = BUS_INPUT;
	}
	uint16_t value = BUS_READ_PIN(DR00_PORT, DR00_PIN) |
			BUS_READ_PIN(DR01_PORT, DR01_PIN) << 1 |
			BUS_READ_PIN(DR02_PORT, DR02_PIN) << 2 |
			BUS_READ_PIN(DR03_PORT, DR03_PIN) << 3 |
			BUS_READ_PIN(DR04_PORT, DR04_PIN) << 4 |
			BUS_READ_PIN(DR05_PORT, DR05_PIN) << 5 |
			BUS_READ_PIN(DR06_PORT, DR06_PIN) << 6 |
			BUS_READ_PIN(DR07_PORT, DR07_PIN) << 7 |
			BUS_READ_PIN(DR08_PORT, DR08_PIN) << 8 |
			BUS_READ_PIN(DR09_PORT, DR09_PIN) << 9 |
			BUS_READ_PIN(DR10_PORT, DR10_PIN) << 10 |
			BUS_READ_PIN(DR11_PORT, DR11_PIN) << 11 |
			BUS_READ_PIN(DR12_PORT, DR12_PIN) << 12 |
			BUS_READ_PIN(DR13_PORT, DR13_PIN) << 13 |
			BUS_READ_PIN(DR14_PORT, DR14_PIN) << 14 |
			BUS_READ_PIN(DR15_PORT, DR15_PIN) << 15;

	return value;
}

void set_sa_addr(uint8_t addr)
{
	switch(addr)
	{
		case SA_ADC:
			SET_SA0(0);
			SET_SA1(0);
			SET_SA2(0);
			break;
		case SA_DK:
			SET_SA0(0);
			SET_SA1(1);
			SET_SA2(0);
			break;
		case SA_BUS:
			SET_SA0(0);
			SET_SA1(0);
			SET_SA2(1);
			break;
		default:
			break;
	}
}

void latch_read(void)
{
	IOW_SET_HIGH();
	delay_ns(400);
	IOR_SET_LOW();
	delay_ns(400);
	IOR_SET_HIGH();
}

void latch_write(uint16_t out)
{
	IOR_SET_HIGH();
	delay_ns(400);
	IOW_SET_LOW();
	delay_ns(400);
	bus_write(out);
	IOW_SET_HIGH();
}

// WHEN IOR LOW
uint16_t iomax_read_adc(void)
{
	set_sa_addr(SA_ADC);
	latch_read();
	return bus_read();
}

uint8_t iomax_read_cabin_state_and_reset_adc(void)
{
	set_sa_addr(SA_DK);
	latch_read();
	return bus_read();
}

uint16_t iomax_read_discrete_input(void)
{
	set_sa_addr(SA_BUS);
	latch_read();
	return bus_read();
}

void iomax_start_adc(uint16_t out)
{
	set_sa_addr(SA_ADC);
	latch_write(out);
}

void iomax_write_tiristor(uint8_t out)
{
	set_sa_addr(SA_DK);
	latch_write(out);
}

void iomax_write_discrete_output(uint16_t out)
{
	set_sa_addr(SA_BUS);
	latch_write(out);
}
