#ifndef __IOMAX__
#define __IOMAX__ 1

#include "stm32f4xx.h"
#include "stm32f4xx_hal.h"
#include "stm32f4xx_hal_gpio.h"

/*
port chosing without ltdc and sdram intersection
--------------
IOR 	- PD4
IOW 	- PD5
SA1 	- PD6
SA2 	- PD7
---------------
DR00 	- PG9
DR01	- PG10
DR02	- PG11
DR03	- PG12
DR04	- PG13
DR05	- PG14
DR06	- PB3
DR07	- PB4
DR08	- PB5
DR09	- PB6
DR10	- PB7
DR11	- PE2
DR12	- PE3
DR13	- PE4
DR14	- PE5
DR15	- PE6
---------------
*/

#ifndef UNITTEST
	// XP7_1 
	#define IOR_PORT	GPIOD
	#define IOR_PIN 	GPIO_PIN_4
	// XP7_2
	#define IOW_PORT 	GPIOD
	#define IOW_PIN 	GPIO_PIN_5
	// XP7_3 
	#define SA0_PORT 	// CONNECT TO GND
	#define SA0_PIN 	// ALWAYS ZERO
	// XP7_5 
	#define SA1_PORT 	GPIOD
	#define SA1_PIN 	GPIO_PIN_6
	// XP7_7 
	#define SA2_PORT  	GPIOD
	#define SA2_PIN 	GPIO_PIN_7

	// XP6
	#define DR00_PORT  	GPIOG
	#define DR00_PIN	GPIO_PIN_9
	#define DR01_PORT  	GPIOG
	#define DR01_PIN 	GPIO_PIN_10
	#define DR02_PORT  	GPIOG
	#define DR02_PIN 	GPIO_PIN_11
	#define DR03_PORT  	GPIOG
	#define DR03_PIN 	GPIO_PIN_12
	#define DR04_PORT  	GPIOG
	#define DR04_PIN 	GPIO_PIN_13
	#define DR05_PORT  	GPIOG
	#define DR05_PIN 	GPIO_PIN_14
	#define DR06_PORT  	GPIOB
	#define DR06_PIN 	GPIO_PIN_3
	#define DR07_PORT  	GPIOB
	#define DR07_PIN 	GPIO_PIN_4
	#define DR08_PORT  	GPIOB
	#define DR08_PIN 	GPIO_PIN_5
	#define DR09_PORT  	GPIOB
	#define DR09_PIN 	GPIO_PIN_6
	#define DR10_PORT  	GPIOB
	#define DR10_PIN 	GPIO_PIN_7
	#define DR11_PORT  	GPIOE
	#define DR11_PIN 	GPIO_PIN_2
	#define DR12_PORT  	GPIOE
	#define DR12_PIN 	GPIO_PIN_3
	#define DR13_PORT  	GPIOE
	#define DR13_PIN 	GPIO_PIN_4
	#define DR14_PORT  	GPIOE
	#define DR14_PIN 	GPIO_PIN_5
	#define DR15_PORT  	GPIOE
	#define DR15_PIN 	GPIO_PIN_6
#else // FOR TEST
	// XP7_1 
	#define IOR_PORT	GPIOD
	#define IOR_PIN 	GPIO_PIN_4
	// XP7_2
	#define IOW_PORT 	GPIOD
	#define IOW_PIN 	GPIO_PIN_5
	// XP7_3 
	#define SA0_PORT 	// CONNECT TO GND
	#define SA0_PIN 	// ALWAYS ZERO
	// XP7_5 
	#define SA1_PORT 	GPIOD
	#define SA1_PIN 	GPIO_PIN_6
	// XP7_7 
	#define SA2_PORT  	GPIOD
	#define SA2_PIN 	GPIO_PIN_7

	// XP6
	#define DR00_PORT  	GPIOG
	#define DR00_PIN	GPIO_PIN_0
	#define DR01_PORT  	GPIOG
	#define DR01_PIN 	GPIO_PIN_1
	#define DR02_PORT  	GPIOG
	#define DR02_PIN 	GPIO_PIN_2
	#define DR03_PORT  	GPIOG
	#define DR03_PIN 	GPIO_PIN_3
	#define DR04_PORT  	GPIOG
	#define DR04_PIN 	GPIO_PIN_4
	#define DR05_PORT  	GPIOG
	#define DR05_PIN 	GPIO_PIN_5
	#define DR06_PORT  	GPIOB
	#define DR06_PIN 	GPIO_PIN_6
	#define DR07_PORT  	GPIOB
	#define DR07_PIN 	GPIO_PIN_7
	#define DR08_PORT  	GPIOB
	#define DR08_PIN 	GPIO_PIN_8
	#define DR09_PORT  	GPIOB
	#define DR09_PIN 	GPIO_PIN_9
	#define DR10_PORT  	GPIOB
	#define DR10_PIN 	GPIO_PIN_10
	#define DR11_PORT  	GPIOE
	#define DR11_PIN 	GPIO_PIN_11
	#define DR12_PORT  	GPIOE
	#define DR12_PIN 	GPIO_PIN_12
	#define DR13_PORT  	GPIOE
	#define DR13_PIN 	GPIO_PIN_13
	#define DR14_PORT  	GPIOE
	#define DR14_PIN 	GPIO_PIN_14
	#define DR15_PORT  	GPIOE
	#define DR15_PIN 	GPIO_PIN_15
#endif

#ifdef UNITTEST
	uint8_t IOW, IOR;
	uint16_t BUS;
	uint8_t SA0, SA1, SA2;

	#define IOW_READ() IOW
	#define IOR_READ() IOR
	#define BUS_READ_PIN(PORT, PIN) ((BUS & PIN) ? 1 : 0)
	#define BUS_WRITE_PIN(PORT, PIN, PinState) BUS = (PinState) ? BUS | PIN : BUS & ~PIN; // HAL_GPIO_WritePin(PORT, PIN, PinState)

	#define IOW_SET_HIGH() 	IOW = 1; // HAL_GPIO_WritePin(IOW_PORT, IOW_PIN, GPIO_PIN_SET)
	#define IOW_SET_LOW()	IOW = 0; // HAL_GPIO_WritePin(IOW_PORT, IOW_PIN, GPIO_PIN_RESET)
	#define IOR_SET_HIGH() 	IOR = 1; // HAL_GPIO_WritePin(IOR_PORT, IOR_PIN, GPIO_PIN_SET)
	#define IOR_SET_LOW() 	IOR = 0; // HAL_GPIO_WritePin(IOR_PORT, IOR_PIN, GPIO_PIN_RESET)

	#define SET_SA0(STATE) (SA0 = STATE) // HAL_GPIO_WritePin(SA0_PORT, SA0_PIN, STATE)
	#define SET_SA1(STATE) (SA1 = STATE) // HAL_GPIO_WritePin(SA1_PORT, SA1_PIN, STATE)
	#define SET_SA2(STATE) (SA2 = STATE) // HAL_GPIO_WritePin(SA2_PORT, SA2_PIN, STATE)
#else
	#define IOW_READ() HAL_GPIO_ReadPin(IOW_PORT, IOW_PIN)
	#define IOR_READ() HAL_GPIO_ReadPin(IOR_PORT, IOR_PIN)

	#define BUS_READ_PIN(PORT, PIN) HAL_GPIO_ReadPin(PORT, PIN)
	#define BUS_WRITE_PIN(PORT, PIN, PinState) HAL_GPIO_WritePin(PORT, PIN, PinState)

	#define IOW_SET_HIGH() 	HAL_GPIO_WritePin(IOW_PORT, IOW_PIN, GPIO_PIN_SET)
	#define IOW_SET_LOW()	HAL_GPIO_WritePin(IOW_PORT, IOW_PIN, GPIO_PIN_RESET)
	#define IOR_SET_HIGH() 	HAL_GPIO_WritePin(IOR_PORT, IOR_PIN, GPIO_PIN_SET)
	#define IOR_SET_LOW() 	HAL_GPIO_WritePin(IOR_PORT, IOR_PIN, GPIO_PIN_RESET)

	#define SET_SA0(STATE) // HAL_GPIO_WritePin(SA0_PORT, SA0_PIN, STATE)
	#define SET_SA1(STATE) HAL_GPIO_WritePin(SA1_PORT, SA1_PIN, STATE)
	#define SET_SA2(STATE) HAL_GPIO_WritePin(SA2_PORT, SA2_PIN, STATE)
#endif

#define SA_ADC 	0x0
#define SA_DK 	0x2 // DOPIK
#define SA_BUS 	0x4

// configure all pins of bus as outputs 
#define BUS_CONFIGURE_OUTPUT() // bus_configure_out()
// // configure all pins of bus as inputs
#define BUS_CONFIGURE_INPUT() // bus_configure_input()

// DO NOT PULL DOWN IOR and IOW SIMULTANEOUSLY
// WHEN IOR LOW
uint16_t iomax_read_adc(void);
uint8_t  iomax_read_cabin_state_and_reset_adc(void);
uint16_t iomax_read_discrete_input(void);
// WHEN IOW LOW
void	iomax_start_adc(uint16_t out);
void 	iomax_write_tiristor(uint8_t out);
void	iomax_write_discrete_output(uint16_t out);

#endif /*__IOMAX__*/
