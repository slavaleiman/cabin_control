#ifndef __CIRCULAR_BUFFER_H__
#define __CIRCULAR_BUFFER_H__

#include <stdbool.h>

typedef struct circular_buffer_t circular_buffer_t;

void cbuf_init(circular_buffer_t* cb, uint8_t* buffer, size_t size);
void cbuf_free(circular_buffer_t* cbuf);
void cbuf_reset(circular_buffer_t* cbuf);
void cbuf_put(circular_buffer_t* cbuf, uint8_t* data, size_t len);
int cbuf_put2(circular_buffer_t* cbuf, uint8_t* data, size_t len);
int cbuf_get(circular_buffer_t* cbuf, uint8_t* data, size_t len);
bool cbuf_empty(circular_buffer_t* cbuf);
bool cbuf_full(circular_buffer_t* cbuf);
size_t cbuf_capacity(circular_buffer_t* cbuf);
size_t cbuf_size(circular_buffer_t* cbuf);

#endif //__CIRCULAR_BUFFER_H__