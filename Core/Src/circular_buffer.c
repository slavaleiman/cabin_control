#include <stdlib.h>
#include <stdint.h>
#include <stddef.h>

#include "circular_buffer.h"

struct circular_buffer_t {
	uint8_t *buffer;
	size_t head;
	size_t tail;
	size_t max;
	bool full;
};

static void advance_pointer(circular_buffer_t* cbuf)
{
	if(!cbuf)
		return;

	if(cbuf->full)
    {
        cbuf->tail = (cbuf->tail + 1) % cbuf->max;
    }

	cbuf->head = (cbuf->head + 1) % cbuf->max;

	// We mark full because we will advance tail on the next time around
	cbuf->full = (cbuf->head == cbuf->tail);
}

static void retreat_pointer(circular_buffer_t* cbuf)
{
	if(!cbuf)
		return;	

	cbuf->full = false;
	cbuf->tail = (cbuf->tail + 1) % cbuf->max;
}

void cbuf_init(circular_buffer_t** cbuf, uint8_t* buffer, size_t size)
{
	if(!buffer || !size)
		return;

	cbuf->buffer = buffer;
	cbuf->max = size;
	circular_buffer_reset(cbuf);

	assert(circular_buffer_empty(cbuf));

	return cbuf;
}

void cbuf_free(circular_buffer_t* cbuf)
{
	assert(cbuf);
	free(cbuf);
}

void cbuf_reset(circular_buffer_t* cbuf)
{
    assert(cbuf);

    cbuf->head = 0;
    cbuf->tail = 0;
    cbuf->full = false;
}

size_t cbuf_size(circular_buffer_t* cbuf)
{
	assert(cbuf);

	size_t size = cbuf->max;

	if(!cbuf->full)
	{
		if(cbuf->head >= cbuf->tail)
		{
			size = (cbuf->head - cbuf->tail);
		}
		else
		{
			size = (cbuf->max + cbuf->head - cbuf->tail);
		}

	}

	return size;
}

size_t cbuf_capacity(circular_buffer_t* cbuf)
{
	assert(cbuf);

	return cbuf->max;
}

void cbuf_put(circular_buffer_t* cbuf, uint8_t data)
{
	assert(cbuf && cbuf->buffer);

    cbuf->buffer[cbuf->head] = data;

    advance_pointer(cbuf);
}

int cbuf_put2(circular_buffer_t* cbuf, uint8_t data)
{
    int r = -1;

    assert(cbuf && cbuf->buffer);

    if(!circular_buffer_full(cbuf))
    {
        cbuf->buffer[cbuf->head] = data;
        advance_pointer(cbuf);
        r = 0;
    }

    return r;
}

int cbuf_get(circular_buffer_t* cbuf, uint8_t * data)
{
    assert(cbuf && data && cbuf->buffer);

    int r = -1;

    if(!circular_buffer_empty(cbuf))
    {
        *data = cbuf->buffer[cbuf->tail];
        retreat_pointer(cbuf);

        r = 0;
    }

    return r;
}

bool cbuf_empty(circular_buffer_t* cbuf)
{
	assert(cbuf);

    return (!cbuf->full && (cbuf->head == cbuf->tail));
}

bool cbuf_full(circular_buffer_t* cbuf)
{
	assert(cbuf);

    return cbuf->full;
}